# GIT-L: A shortcut for git log command

This is as simple as it could be, just a shortcut for the command:
`git log --oneline --graph --color --all --decorate` 

Nothing more than that.

## Installing
I recommend installing it globally
On Windows: `npm i -g git-l`
On Mac/Linux: `sudo npm i -g git-l`

## Usage
Simply cd into your repo folder and:
`git-l`

### Enjoy you day typing less!